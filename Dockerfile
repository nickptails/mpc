ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION} AS builder

ARG ALPINE_VERSION
ARG MPC_VERSION_MAJOR
ARG MPC_VERSION_MINOR

RUN apk add --no-cache \
        build-base \
        curl \
        libmpdclient-dev \
        meson && \
    curl -fLo mpc-${MPC_VERSION_MAJOR}.${MPC_VERSION_MINOR}.tar.xz \
        "https://www.musicpd.org/download/mpc/${MPC_VERSION_MAJOR}/mpc-${MPC_VERSION_MAJOR}.${MPC_VERSION_MINOR}.tar.xz" && \
    tar xJf mpc-${MPC_VERSION_MAJOR}.${MPC_VERSION_MINOR}.tar.xz && \
    cd mpc-${MPC_VERSION_MAJOR}.${MPC_VERSION_MINOR} && \
    mkdir -p build && \
    meson build \
        -Dtest=false \
        -Ddocumentation=disabled && \
    DESTDIR=/mpc ninja -C build install


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /mpc/usr/local/bin/ /usr/local/bin/
RUN apk add --no-cache \
        libmpdclient

CMD ["mpc"]
